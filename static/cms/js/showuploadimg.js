/**
 * Created by pal_player on 16/12/22.
 */

$().ready(function(){
    $("input[type='file']").each(function(i,item){
        $(item).bind("change",function(){
            zipImage($(item).attr("id"));
        })
    })
})

function zipImage(id){
    var file = document.getElementById(id).files[0];
    var fReader = new FileReader();
    var complate = function(){};
    fReader.onload = function(file){
        var canvas = document.createElement("canvas");
        $("#"+id+"_div").append(canvas);
        var img = new Image();
        img.onload = function(){
            if(img.height>300){
                img.width = 300/img.height*img.width;
                img.height = 300;
            }
            var imgCtx = canvas.getContext("2d");
            canvas.width = img.width;
            canvas.height = img.height;
            imgCtx.clearRect(img,0,0,img.width,img.height);
            imgCtx.drawImage(img,0,0,img.width,img.height);
            complate(canvas.toDataURL("image/jpeg"));
        }
        img.src = file.target.result;
    }
    fReader.readAsDataURL(file);
}
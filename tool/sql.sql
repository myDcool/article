CREATE TABLE `user` (
              `uid` int(11) NOT NULL AUTO_INCREMENT,
              `status` tinyint(4) NOT NULL DEFAULT '0',
              `role` tinyint(4) NOT NULL DEFAULT '0',
              `username` varchar(15) NOT NULL DEFAULT '',
              `mobile` varchar(11) NOT NULL DEFAULT '',
              `password` varchar(60) NOT NULL DEFAULT '',
              `addtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
              `reg_from` varchar(20) NOT NULL DEFAULT '' COMMENT '用户注册来源',
              PRIMARY KEY (`uid`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
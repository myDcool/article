@echo off

rem 启动进程前杀掉已有进程
rem taskkill /f /im php-cgi.exe

set currentDir=%cd%

set "phpCGI=%currentDir%\php72\php-cgi.exe"
set "phpCLI=%currentDir%\php72\php.exe"
set "phpConf=%currentDir%\php72\php.ini"
set "docroot=%currentDir%\code\your_doc_root"
set "entryfile=%docroot%\phpserver.php"

%phpCLI% -v
@echo on

%phpCLI% -S localhost:9721 -c %phpConf% -t %docroot% %entryfile%


<?php

/**
 * desc 获取请求参数, 只检查数据的合法性, 并不改变数据内容
 * Class Request
 */
class Request
{
    public static $data = false;
    public $isValid = false;

    public static function Post($key, $default=NULL)
    {
        self::$data = isset($_POST[$key]) ? $_POST[$key] : $default;
        self::valid($key);

        return self::$data;
    }
    
    public static function Get($key, $default=NULL)
    {
        self::$data = isset($_GET[$key]) ? $_GET[$key] : $default;
        self::valid($key);

        return self::$data;
    }

    public static function Cookie($key, $default=NULL)
    {
        self::$data = isset($_COOKIE[$key]) ? $_COOKIE[$key] : $default;
        self::valid($key);

        return self::$data;
    }

    public static function Route($key, $default=NULL)
    {
        self::$data = isset(Route::$args[$key]) ? Route::$args[$key] : $default;
        self::valid($key);

        return self::$data;
    }
    
    public static function Server($key, $default=NULL)
    {
        self::$data = isset($_SERVER[$key]) ? $_SERVER[$key] : $default;
        return self::$data;
    }
    
    public static function Session($key, $default=NULL)
    {
        self::$data = isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
        return self::$data;
    }

    /**
     * desc 检测数据的合法性
     * @param string $key 参数键
     * @param string $value 参数值
     * @throws Exception
     */
    public static function valid($key, $value='')
    {
        $value = !empty($value) ? $value : self::$data;
        
        if (!is_null($value)) {
            if (!is_array($value)) {
                $value = array($value);
            }
            foreach ($value as $v) {
                VerifyConfig::verify($key, $v, MODULE_NAME, CONTROLLER_NAME, ACTION_NAME);
            }
        }
    }

    public static function isMobile()
    {
        //...
    }
    
    public static function isPost()
    {
        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /**
     * 获取当前访问的URL
     */
    public static function Url()
    {
        return $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    }
    
    /**
     * 一般获得用户IP都是使用$_SERVER['REMOTE_ADDR']这个环境变量，但是此变量只会纪录最后一个主机IP，所以当用户浏览器有设定Proxy时，就无法取得他的真实IP。
     * 这时可以使用另一个环境变量$_SERVER['HTTP_X_FORWARDED_FOR']，它会纪录所经过的主机IP，但是只有在用户有透过Proxy时才会产生,
     * 而且需要webserver的支持
     * @return string 客户端ip
     */
    public static function getClientIp()
    {
        $REMOTE_ADDR = !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
        // $HTTP_CLIENT_IP = !empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : false;
        $ARR_HTTP_X_FORWARDED_FOR = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']) : array('0');
        $HTTP_X_FORWARDED_FOR = $ARR_HTTP_X_FORWARDED_FOR['0'];
    
        $ip = $HTTP_X_FORWARDED_FOR ? $HTTP_X_FORWARDED_FOR : $REMOTE_ADDR;
        // $ip = $HTTP_CLIENT_IP ? $HTTP_CLIENT_IP : $REMOTE_ADDR;
    
        return $ip ? $ip : '0';
    }
    
    /**
     * @return string 服务器ip
     */
    public static function getServerIp()
    {
        $SERVER_ADDR = !empty($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : false;
        return $SERVER_ADDR ? $SERVER_ADDR : '0';
    }
    
    /**
     * 上传了单个文件
     * @param $formName
     * @return array
     */
    public static function FileOne($formName)
    {
        if (empty($_FILES[$formName]['size'])) {
            return array();
        }
    
        //取得后缀
        $arrFileInfo = pathinfo($_FILES[$formName]['name']);
        return array(
            'name'      => $_FILES[$formName]['name'],
            'type'      => $_FILES[$formName]['type'],
            'size'      => $_FILES[$formName]['size'],
            'tmp_name'  => $_FILES[$formName]['tmp_name'],
            'error'     => $_FILES[$formName]['error'],
            'base_name' => $arrFileInfo['basename'],
            'extension' => $arrFileInfo['extension'],
        );
    }
    
    /**
     * 上传了多个文件
     * @param $formName
     * @return array
     */
    public static function FileMore($formName)
    {
        if (empty($_FILES[$formName])) {
            return array();
        }
    
        $arrImages = array();
        foreach ($_FILES[$formName]['name'] as $k => $name) {
            if (!empty($_FILES[$formName]['size'][$k])) {
                $arrFileInfo = pathinfo($name);
                $arrImages[$k] = array(
                    'name'      => $name,
                    'type'      => $_FILES[$formName]['type'][$k],
                    'size'      => $_FILES[$formName]['size'][$k],
                    'tmp_name'  => $_FILES[$formName]['tmp_name'][$k],
                    'error'     => $_FILES[$formName]['error'][$k],
                    'base_name' => $arrFileInfo['basename'],
                    'extension' => $arrFileInfo['extension'],
                );
            }
        }
    
        return $arrImages;
    }
    
    /**
     * 获取未修改过的提交信息
     * enctype="multipart/form-data" 的时候 php://input 是无效的
     * @return bool|string
     */
    public static function rawInput()
    {
        self::$data = file_get_contents('php://input', 'r');
        
        return self::$data;
    }
    
    /**
     * 获取 $_REQUEST 或者 Route 中的值, 可用于链式调用
     * Request::Input('a')::number(1); //获取参数a的值, 如果是不是数字就返回默认值
     * @param $key
     * @return string
     */
    public static function Input($key)
    {
        self::$data = isset($_REQUEST[$key]) ? $_REQUEST[$key] : NULL;
        self::$data = (self::$data === NULL) ? self::Route($key, NULL) : NULL;
        return self::class;
    }
    
    /**
     * 返回数字数或者默认值
     * @param $default
     * @return bool|int
     */
    public static function number($default=0)
    {
        return is_numeric(self::$data) ? self::$data : $default;
    }
    
    /**
     * 返回数字或者null
     * @return bool|null
     */
    public static function numberOrNull()
    {
        return is_numeric(self::$data) ? self::$data : NULL;
    }
    
    /**
     * 返回数字或者false
     * @return bool
     */
    public static function numberOrFalse()
    {
        return is_numeric(self::$data) ? self::$data : FALSE;
    }
    
}

//http://www.summer.com/index/index/req/a/4route/b?a=1get

//echo '<pre>';
//var_dump(Request::Get('a'));
//var_dump(Request::Get('b'));
//var_dump(Request::Post('a'));
//var_dump(Request::Cookie('a'));
//var_dump(Request::Route('a'));

//string(4) "1get"
//bool(false)
//bool(false)
//bool(false)
//string(6) "4route"
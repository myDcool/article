<?php

/**
 * Trait IError
 * 放在别的类中, 用来集中存放错误信息
 * 出现fatal error 时只用从这里获取失败信息即可
 */
trait IError
{
	public static $_error = [];
	public static $_ext = []; //其它备注信息

	public static function _SetError($str, $key='')
	{
		if (!empty($key)) {
			self::$_error[$key] = $str;
		} else {
			self::$_error[] = $str;
		}
	}
    
    /**
     * @return mixed
     * 获取最后一个错误信息
     */
	public static function _Error()
	{
		return end(self::$_error);
	}
    
    /**
     * @return bool
     * 是否出现了错误信息
     */
	public static function _IsSucceed()
	{
		return count(self::$_error) ? FALSE : TRUE;
	}
    
    /**
     * @return array
     * 其他备注信息
     */
	public static function _Ext()
	{
		return self::$_ext;
	}
    
    /**
     * 输出json化的错误信息
     */
	public static function _ErrorOut()
	{
		exit(json_encode(self::$_error));
	}
}

<?php

/**
* 业务处理基类
* 连接数据库
* 获得分页信息
*/
class Model extends DBmysql
{
    public $row = array();
    
	//分页函数
	public function setpage($num, $currentpage, $baseurl, $pagesize='')
	{
		$pagesize 		= empty($pagesize) ? 10 : $pagesize;			//每页显示多少条记录
		$totalpage 		= ceil($num / $pagesize);					//总共多少页
		$currentpage 	= empty($currentpage) ? 1 : $currentpage;		//当前页
		$lastpage 		= (($currentpage-1) <= 0)? 1: $currentpage-1;	//最后一页
		$nextpage 		= (($currentpage+1)>$totalpage) ? $totalpage : $currentpage+1;//下一页
		// $baseurl 	= empty($baseurl) ? $this->actionUrl : $baseurl;//不带分页的跳转链接
		$firstpageurl 	= $baseurl;
		$lastpageurl 	= $baseurl."/page/{$lastpage}";
		$nextpageurl 	= $baseurl."/page/{$nextpage}";
		$finalpageurl 	= $baseurl."/page/{$totalpage}";
		
		$pageinfo = array(
				'num'			=> $num,
				'currentpage' 	=> $currentpage,
				'totalpage' 	=> $totalpage,
				'firstpageurl' 	=> $firstpageurl,
				'lastpageurl' 	=> $lastpageurl,
				'nextpageurl' 	=> $nextpageurl,
				'finalpageurl' 	=> $finalpageurl);
		
		$start = ($currentpage - 1)*$pagesize;
		$limit = $start.','.$pagesize;
		return array(
			'page' => $pageinfo,
			'limit' => $limit
        );
	}
    
    /**
     * desc 符合条件的记录数, a=1, b=2 ... 等号判断
     * @param $table
     * @param $arr ['a' => 1, 'b' => 2]
     * @return bool|string
     */
    public static function isExists($table, $arr)
    {
        $count = self::link($table)
            ->where($arr)
            ->count();
        
        return $count > 0 ? $count : FALSE;
    }
    
    /**
     * 通过主键id获取一行记录
     * @param $id
     * @return string
     */
    public function getRowById($id)
    {
        $this->row = $this->where(['id' => $id])->limit(1)->select()->getOne();
        return $this->row;
    }
    
    public static function getGlobalId($project=1)
    {
//        CREATE TABLE `global_id` (
//            `id` int(11) NOT NULL AUTO_INCREMENT,
//            `project` tinyint(4) NOT NULL DEFAULT '0' COMMENT '来源项目编号',
//            PRIMARY KEY (`id`)
//        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    
        // 本项目编号 1
        return self::link('gid')
            ->insert(['project' => $project])
            ->insertId;
    }
    
    //将join语句拆分成两次独立查询
    //利用到CRUD组装SQL时的数据
    public static function getJoinResult()
    {
    
    }
    
    //用来查询被分库分表的历史数据
    //用作数据查询的中间层, 自动判断是否要查到历史数据
    public static function getHistoryData()
    {
    
    }
    
}
<?php

/**
 * Trait DBCache
 * 缓存数据, db查询结果, 临时数据等等
 */
class RunCache
{
    public static $_Cache = array();
    
    public static function clearCache($key)
    {
        unset(self::$_Cache[$key]);
    }
    
    public static function addCache($key, $value)
    {
        self::$_Cache[$key] = $value;
    }
    
    public static function getCache($key)
    {
        if (!isset(self::$_Cache[$key])) {
            return NULL;
        } else {
            return self::$_Cache[$key];
        }
    }
    
    public static function isExists($key)
    {
        return isset(self::$_Cache[$key]) ? TRUE : FALSE;
    }
}
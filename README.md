## Summer Article
- 新版：http://article.hearu.top ， 手机端优先版浏览和管理后台， 集成UI友好的评论功能

#### 功能
- 管理后台可以对文章增删改查
- 角色与权限管理
- 前台文章信息展示
- 前台评论与回复: 多级回复楼梯式缩进

#### 技术栈

- 后端: LNMP, 其中PHP使用 SummerPHP 框架
- 前端: vue + jquery + amazeui

#### nginx配置 单一入口
```
server {
        listen       80;
        server_name  www.hearu.top;

                location ~ \.ico|jpg|gif|png|js|css|txt|csv|woff2$ {
            root /path-to-frame/static;
            #expires 1h;
        }

        location / {
            root /path-to-frame/;
            fastcgi_pass   127.0.0.1:9000;
            #fastcgi_index  index.php;
            fastcgi_param  SCRIPT_FILENAME  $document_root/index.php;
            include        fastcgi_params;
        }

    }
```

#### [界面展示](http://www.cnblogs.com/iLoveMyD/p/6523154.html)

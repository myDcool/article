<?php

class Role extends Model
{
	/**
	 * desc 获取用户权限列表
	 * @param int $uid
	 * @return array
     * @throws Exception
	 */
	public static function getUserPrivilege($uid)
	{
		$roleid = self::link('role_bind')
            ->where(['uid' => $uid])
            ->select()
            ->getOneValue('roleid');

		if (empty($roleid)) {
			$access =  self::link('role')
                ->where(['id' => 1])
                ->select()
                ->getOneValue('access'); //默认为普通用户的权限
		} else {
			$access =  self::link('role')
                ->where(['id' => $roleid])
                ->select()
                ->getOneValue('access');
		}

		$arrAccess = json_decode($access, TRUE);
		empty($arrAccess) && $arrAccess = [];
		$previlege = [];
		foreach ($arrAccess as $ctrl => $actions) {
			foreach ($actions as $action) {
				$previlege[$ctrl.'_'.$action] = 1;
			}
		}
		return $previlege;
	}

}
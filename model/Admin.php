<?php

class Admin extends Model
{
	public static $Status_Ok = 1; //正常
	public static $Status_Hide = -1; //隐藏
	public static $Status_Del = -2; //删除, 后台也不显示

	public static $StatusCn = [
		'1' => '正常', '-1' => '隐藏'
	];

	public static $Top = [
		'-1' => '不置顶', '1' => '置顶1', '2' => '置顶2', '3' => '置顶3'
	];
}
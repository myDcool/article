<?php
class Bbs extends Model
{
	public static $replayList = array();
	public static $tieziList = array();
	public static $sort = array();
	public static $rootid = 0;
	
	//获取论坛文章列表
	public static function getTiezi()
	{
		$r = self::link('tiezi')
			->where(['level' => 0])
			->fields('id, strtime, content, nickname')
			->order('id desc')
			->select()
            ->getAll();
		return $r;
	}

	//获取一级回复, 这里是获取帖子的所有第一层回复
	public function getLv1Replays($rootid)
	{
		$where = [
			'rootid' => $rootid,
			'level' => 1
		];
		return self::link('tiezi')->where($where)->select($where);
	}

//	//获取帖子详情
//	//次函数以及下边的rv函数一起使用, 但是不推荐这种获取回复列表的方法
//	public static function getTieziDetail($rootid)
//	{
//		self::$rootid = $rootid;
//		//获得首贴信息, 相当于论坛中的文章
//		// $fields = 'id first_childid content strtime';
//		$root = self::link('tiezi')
//			->where(['id' => $rootid])
//			->getOne();
//		//获取所有回复信息
//		self::$tieziList = self::link('tiezi')
//			->where(['rootid' => $rootid])
//			->order('id')
//			->select('id');//以id为建
//
//		self::rv($root);
//		self::$tieziList[$rootid] = $root;
//		unset(self::$sort[0]);
//
//		return array(
//			'tiezi' => self::$tieziList,
//			'root' => $root,
//			'sort' => self::$sort
//			);
//	}
//
//	//非递归实现, 一次性将所有回复都读出来, 然后按照回复层级关系重新组装
//	//每次插入时，将自己以及自己的第一个和最后一个孩子节点,下一个兄弟节点同时插入
//	public static function rv($root)
//	{
//		self::$sort[] = $root['id'];
//		self::$sort[] = $root['first_childid'];
//		self::$sort[] = $root['last_childid'];
//
//		self::$sort = array_unique(self::$sort);
//
//		foreach (self::$tieziList as $currentid => $v) {
//			$currentid_key = array_search($currentid, self::$sort); //判断当前节点是否已经插入sort数组
//			// if ($currentid_key) { //貌似当前节点肯定存在于$this->sort中
//				$first_childid = $v['first_childid'];
//				$last_childid = $v['last_childid'];
//				$next_brotherid = $v['next_brotherid'];
//
//				//插入第一个子节点和最后一个子节点
//				if ($first_childid && ($first_childid != self::$sort[$currentid_key+1])) { //如果其第一个子节点不在sort中，就插入
//					array_splice(self::$sort, $currentid_key + 1, 0, $first_childid);
//					if ($last_childid && ($last_childid != $first_childid)) { //只有一条回复时，first_childid  == last_childid
//						array_splice(self::$sort, $currentid_key + 2, 0, $last_childid); //插入最后一个子节点
//					}
//				}
//
//				//插入兄弟节点
//				if ($next_brotherid) { //存在才插入
//					$next_brotherid_key = array_search($next_brotherid, self::$sort);
//					if (!$next_brotherid_key) { // 只有两条回复时，下一个兄弟节点肯定已经插入了
//						if ($last_childid) {
//							$last_childid_key = array_search($last_childid, self::$sort);
//							array_splice(self::$sort, $last_childid_key + 1, 0, $next_brotherid); //将下一个兄弟节点插入到最后一个子节点后边
//						} elseif ($first_childid) {
//							array_splice(self::$sort, $currentid_key + 2, 0, $next_brotherid); //将下一个兄弟节点插入到第一个子节点后边
//						} else {
//							array_splice(self::$sort, $currentid_key + 1, 0, $next_brotherid); //将下一个兄弟节点插入到本节点后边
//						}
//					}
//				}
//			// }
//		}
//	}

	/**
	 * desc 添加回复
	 * @param $rootid
	 * @param $fatherid
	 * @param $content
     * @throws Exception
	 */
	public static function addReplay($rootid, $fatherid, $content)
	{
		//获取父帖的信息
		$r = self::link('tiezi')
            ->where(['id' => $fatherid, 'rootid' => $rootid])
            ->select()
            ->getOne();
		if (empty($r)) {
			Response::redirect('未找到您要回复的帖子, 可能违规被删除!', CONTROLLER_URL);
		}

		$first_childid = $r['first_childid'];
		$last_childid = $r['last_childid'];
		$level = $r['level'];

		//插入本次数据
		$a = array(
			'fatherid' => $fatherid,
			'pre_brotherid' => $last_childid,
			'next_brotherid' => 0,
			'first_childid' => 0,
			'last_childid' => 0,
			'inttime' => REQUEST_TIME,
			'content' => $content,
			);

		//如果父帖是首帖(level == 0)
		$a['rootid'] = $level ? $rootid : $fatherid;
		$a['level'] = ++$level;

		$insert_id = self::link('tiezi')->insert($a)->insertId;

		//判断是否是沙发帖, 是的话, 在主帖中记录下来, 不是沙发的话, 修改上一个回复帖的next_brotherid, 最后修改父帖的last_childid为本帖id
		if ($first_childid) { //非沙发贴
			self::link('tiezi')
				->where(['id' => $last_childid])
				->updateVal(['next_brotherid' => $insert_id])
                ->update();

			//修改父帖的last_childid为本帖
			self::link('tiezi')
				->where(['id' => $fatherid])
				->updateVal(['last_childid' => $insert_id])
                ->update();
		} else { //沙发帖
			self::link('tiezi')
				->where(['id' => $fatherid])
				->updateVal(['first_childid' => $insert_id, 'last_childid' => $insert_id])
                ->update();
		}
	}

	//获取第一级回复
	public static function getFirstReplays($rootid)
	{
		return self::link('tiezi')
			->fields('id, rootid, fatherid, level, inttime, content')
			->where(['rootid' => $rootid, 'level' => 1])
			->select()
            ->getAll();
	}

	//先根序遍历, 每次只返回某一个回复的所有子回复, 省内存, 但要多次读取数据库
	// 1. 如果某节点有孩子节点, 将该节点压栈, 并访问其第一个孩子节点
	// 2. 如果某节点没有孩子节点, 那么该节点不压栈, 进而判断其是否有兄弟节点
	// 3. 如果有兄弟节点, 访问该节点, 并按照1,2步规则进行处理
	// 4. 如果没有兄弟节点, 说明该节点是最后一个子节点
	// 5. 出栈时, 判断其是否有兄弟节点, 如果有, 则按照1,2,3 进行处理, 如果没有则按照第4步处理, 直到栈为空
	public static function getAllReplaysByRootFirst($id)
	{
		$current = self::link('tiezi')->where(['id' => $id])->getOne();

		$replay = []; //遍历的最终顺序
		$stack = []; //遍历用的栈

		if (!empty($current['first_childid'])) {
			//因为刚开始 $stack 肯定是空的, 而且也不知道该树是否只有根节点, 所以用do...while
			do {
				if (empty($current['stack'])) { // 不是保存在栈里的元素
					$replay[] = $current;
					if (!empty($current['first_childid'])) { //有孩子节点, 就把current替换为孩子节点, 并记录信息
						$current['stack'] = 1; 
						$stack[] = $current;
						$current = self::link('tiezi')->where(['id' => $current['first_childid']])->getOne();
					} elseif (!empty($current['next_brotherid'])) { // 没有孩子节点, 但是有兄弟节点, 就把
						$current = self::link('tiezi')->where(['id'=>$current['next_brotherid']])->getOne();
					} else {
						$current = array_pop($stack);
					}
				} else { // 是栈里(回溯)的元素, 只用判断其有没有兄弟节点就行了
					if (!empty($current['next_brotherid'])) { // 没有孩子节点, 但是有兄弟节点, 就把
						$current = self::link('tiezi')->where(['id' => $current['next_brotherid']])->getOne();
					} else {
						$current = array_pop($stack);
					}
				}
				
			} while (!empty($stack));
		}

		return $replay;
	}

	/**
	 * desc 仅获取第一层子回复
	 * @param $fatherid
	 * @return mixed
	 */
	public static function getFirstSubReplay($fatherid)
	{
		$rs = self::link('tiezi')
			->where(['fatherid' => $fatherid])
			->order('inttime')
			->select()
            ->getAll();
		return $rs;
	}

}
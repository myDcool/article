--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cat_id` tinyint(1) unsigned zerofill DEFAULT NULL COMMENT '外键关联article_cats',
  `title` varchar(200) NOT NULL COMMENT '标题',
  `content` longtext COMMENT '文章内容',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，1正常，-1删除',
  `sort` int(11) DEFAULT '0' COMMENT '文章排序',
  `top` int(11) DEFAULT '0',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '新闻生成时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Table structure for table `article_cats`
--

DROP TABLE IF EXISTS `article_cats`;
CREATE TABLE `article_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL COMMENT '分类名',
  `num` tinyint(4) NOT NULL COMMENT '该分类在首页显示文章的条数',
  `status` tinyint(4) NOT NULL COMMENT '是否显示: 1: 显示 0: 隐藏 -1:删除',
  `sort` tinyint(4) NOT NULL COMMENT '排序',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文章分类表';

--
-- Table structure for table `global_id`
--

DROP TABLE IF EXISTS `global_id`;
CREATE TABLE `global_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project` tinyint(4) NOT NULL DEFAULT '0' COMMENT '来源项目编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;


--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '0' COMMENT '角色名',
  `access` text NOT NULL COMMENT '该角色可访问的函数列表json',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='权限管理的角色表';

--
-- Table structure for table `role_bind`
--

DROP TABLE IF EXISTS `role_bind`;
CREATE TABLE `role_bind` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '状态',
  `username` varchar(45) NOT NULL DEFAULT '0' COMMENT '用户姓名',
  `roleid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色与uid的绑定关系';
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `tiezi`
--

DROP TABLE IF EXISTS `tiezi`;
CREATE TABLE `tiezi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rootid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '首帖id',
  `fatherid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父帖id',
  `pre_brotherid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上一个兄弟节点',
  `next_brotherid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下一个兄弟帖id',
  `first_childid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '第一条回帖id',
  `last_childid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后一个回复帖的id',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '本帖深度(第几层回复)',
  `inttime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发帖时间戳',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发帖字符时间',
  `nickname` varchar(15) DEFAULT '0',
  `content` varchar(500) NOT NULL DEFAULT '0' COMMENT '帖子内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `addtime` int(11) unsigned DEFAULT '0' COMMENT '添加时间戳',
  `status` tinyint(4) DEFAULT '0' COMMENT '状态, 0:待审核 1:审核通过 5:禁言 6:拉黑',
  `username` varchar(30) NOT NULL DEFAULT '',
  `mobile` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(30) DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `reg_from` varchar(20) DEFAULT '',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

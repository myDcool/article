<?php

class RouteConfig
{
    public static $Path = array(
    
        'user_login' => 'user/login',
        'user_register' => 'user/register',
        'user_logout' => 'user/logout',
        
        'getArticleList' => 'home/index/getArticleList',
        'detail_(\d+)' => 'home/index/detail/articleid/$1',
        'articleDetail_(\d+)' => 'home/index/articleDetail/articleid/$1',
        'replay' => 'home/index/replay',
        'getFirstReplays_(\d+)' => 'home/index/getFirstReplays/articleid/$1',
        'getAllReplay' => 'home/index/getAllReplay/articleid/$1',
    );
}
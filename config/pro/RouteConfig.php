<?php

class RouteConfig
{
    public static $Path = array(
        
        'user_login' => 'user/login',
        'user_register' => 'user/register',
        'user_logout' => 'user/logout',
        
        'article_list_(\d+)' => 'index/index/route/page/$1'
    );
}
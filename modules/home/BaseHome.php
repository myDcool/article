<?php

/**
 * desc 前台用户访问基类
 * Class BaseHome
 */
class BaseHome
{
	protected $uid = false;
	protected $userinfo = [];
	protected $loginExpire = 3600; //登录有效期
	protected $notNull = []; //不能为空的数据

	public function __construct()
	{
		$user = User::getUserCookie();
		if (!empty($user)) {
			$this->uid = $user['uid'];
			$this->userinfo = $user;
			View::$arrTplVar['userinfo'] = $user;
			User::setUserCookie($user, $this->loginExpire); //重新计算有效期, 这种写法使得用户的登录有效期总是loginExpire得值
		}

		//预渲染
		View::preShow('header');
		View::preShow('nav');
		View::endShow('footer');
	}

	/**
	 * desc 格式化文章
	 * @param string $content 文章内容
	 * @return string
	 */
	protected function formatArticle($content)
	{
		$content = html_entity_decode($content);
		$content = nl2br($content);
		return addslashes($content);//如果直接用PHP短标签在html模版中渲染, 就转义一下单双引号, 防止引号冲突
	}

}
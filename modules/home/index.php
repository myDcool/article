<?php
class _index extends BaseHome
{
	public $nickname = '';
	public function initc()
	{
		// $this->userinfo->nickname = $this->getRandName();
		// $rand_num = mt_rand(1, 1389);
		// $r = $this->loadBusiness('Test')->getRandArtName($rand_num);
		// $this->userinfo->nickname = $r['name'];
	}

	public function index()
	{
		View::show('list');
	}

	public function getArticleList()
	{
		$list = Bbs::link('article')
			->fields('id, title, create_time')
			->order('id desc')
			->limit(20)
			->select()
            ->getAll();
		Response::success($list);
	}

	//回复帖子
	public function replay()
	{
		$articleid = Request::post('articleid'); //文章id
		$fatherid = Request::post('fatherid'); //上级回复的id
		$content = Request::post('content');

		if (!empty($content)) {
			Bbs::addReplay($articleid, $fatherid, $content);
			Response::redirect('回复成功~', BASEURL.'detail_'.$articleid);
		} else {
			Response::redirect('回复内容不能为空', BASEURL.'detail_'.$articleid);
		}
	}

	//某一文章的详细回帖
	public function detail()
	{
		$aid = Request::post('articleid');
		View::$arrTplVar['aid'] = $aid;
		View::show('detail');
	}

	//文章详情
	public function articleDetail()
	{
		$aid = Request::Route('articleid');
		$article = Article::link('article')->where(['id' => $aid])->select()->getOne();
		Response::success($article);
	}

	//获取文章的所有一级回复
	public function getFirstReplays()
	{
		$fatherid = Request::Route('articleid');
		$r = Bbs::getFirstReplays($fatherid);
		Response::success($r);
	}

	//获取某一回复的一级子回复
	public function getAllReplay()
	{
		$fatherid = Request::post('fatherid');
//		$r = Bbs::getAllReplaysByRootFirst($fatherid);
//		$r = Bbs::getTieziDetail($fatherid);
		$r = Bbs::getFirstSubReplay($fatherid);
        
        Response::jsonReturn($r);
	}

	//测试swool的代码
	public function test()
	{
		$serv = new Swoole\Websocket\Server("127.0.0.1", 9502);

		$serv->on('Open', function($server, $req) {
			echo "connection open: ".$req->fd;
		});

		$serv->on('Message', function($server, $frame) {
			echo "message: ".$frame->data;
			$server->push($frame->fd, json_encode(["hello", "world"]));
		});

		$serv->on('Close', function($server, $fd) {
			echo "connection close: ".$fd;
		});

		$serv->start();
	}
}

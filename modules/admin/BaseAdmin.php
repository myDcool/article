<?php

/**
 * desc cms基类
 * Class Admin
 */
class BaseAdmin extends Main
{
	protected $adminUid = false;
	protected $adminInfo = [];
	protected $loginExpire = 3600; //管理员登录有效期
	protected $notNull = []; //不能为空的数据

	public function __construct()
	{
		$user = User::getUserCookie();
		if (!empty($user['uid'])) {
			$this->adminUid = $user['uid'];
			$this->adminInfo = $user;
			View::$arrTplVar['adminInfo'] = $this->adminInfo;
			User::setUserCookie($user, $this->loginExpire); //重新计算有效期, 这种写法使得用户的登录有效期总是loginExpire得值
		} elseif (ACTION_NAME != 'login' && ACTION_NAME != 'logout') {
			Response::redirect('请重新登录!', CONTROLLER_URL.'login');
		}

		//预渲染
		View::preShow('public/header');
		View::preShow('public/nav');
		View::preShow('public/siderbar');
		View::endShow('public/footer');

		//赋值
		View::$arrTplVar['VISIT_PATH_NAME'] = '/'.MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;

		//给页面的权限变量赋值 Public/public/public_footer.html
		$this->setPrivilege();

		//验证操作权限
		//$this->checkPrivilege();
	}

	/**
	 * desc 登录
	 */
	public function login()
	{
		//登录成功, 记录redis
		$user = User::getUserCookie();
		if (!empty($user['uid'])) {
			Response::redirect('您已经登录了!', MODULE_URL.'index');
		}

		if (IS_POST) {
			$username = Request::post('username');
			$password = Request::post('password');

			$user = User::verifyUserLogin($username, $password);

			if (empty($user['uid'])) {
				Response::redirect('未找到该用户!', ACTION_URL);
			} else {
				User::setUserCookie($user, $this->loginExpire); //记人cookie
				Response::redirect('登录成功!', MODULE_URL.'article/listArticle');
			}

		}
		View::display('login'); //不会预加载html
    }

	/**
	 * desc 退出
	 */
    public function logout()
	{
		User::clearUserCookie();
		Response::redirect('退出成功!', CONTROLLER_URL.'login');
	}

	/**
	 * desc 公共数据验证
	 * @param array $params 参数
	 */
	protected function verify($params)
	{
		// $number = [];

		//对不能为空的数据进行验证
		foreach ($this->notNull as $arg => $cn) {
			if (empty($params[$arg])) {
				Response::notify('<strong>'.$cn.'不能为空!</strong><br><br>请点击浏览器返回按钮继续操作');
			}
		}

	}

	/**
	 * desc 格式化文章
	 * @param string $content 文章内容
	 * @return string
	 */
	protected function formatArticle($content)
	{
		$content = html_entity_decode($content);
		$content = nl2br($content);
		return $content;//如果直接用PHP短标签在html模版中渲染, 就转义一下单双引号, 防止引号冲突
	}

	/**
	 * desc 获取管理员权限
	 */
	protected function setPrivilege()
	{
		$previlege = Role::getUserPrivilege($this->adminUid);
		View::$arrTplVar['admin_access'] = json_encode($previlege);
	}

	/**
	 * desc 判断用户是否有权限操作
	 * todo 加缓存
	 */
	protected function checkPrivilege()
	{
		$previlege = Role::getUserPrivilege($this->adminUid);
		$action = MODULE_NAME.'_'.CONTROLLER_NAME.'_'.ACTION_NAME;

		if (empty($previlege[$action]) && ACTION_NAME != 'login' && ACTION_NAME != 'logout') {
			Response::notify('非法操作!');
		}
	}
}
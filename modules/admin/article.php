<?php
/**
 * desc 文章管理
 * Class Article
 */
class _article extends BaseAdmin
{
	public $notNull = ['title' => '标题', 'cat_id' => '分类', 'content' => '内容', 'status' => '状态', 'top' => '置顶'];
	/**
	 * desc 添加文章分类
	 */
	public function addArticleType()
	{
		if (IS_POST) {
			$article_cats = [
				'title'		=> Request::post('title'),
				'num'		=> Request::post('num'),
                'sort'		=> Request::post('sort'),
				'status'	=> Admin::$Status_Hide,
				'create_time'	=> date('Y-m-d H:i:s')
			];

			$rs = Article::link('article_cats')->insert($article_cats);

			if ($rs !== FALSE) {
				Response::redirect('添加成功!', CONTROLLER_URL.'listArticleType');
			} else {
				Response::redirect('抱歉, 因网络原因添加失败', ACTION_URL);
			}
		}
		View::show('article_type_add');
	}

	/**
	 * desc 编辑文章分类
	 */
	public function editArticleType()
	{
		$id = Request::post('id');
		if (IS_POST) {
			$article_cats = [
				'title'		=> Request::post('title'),
				'num'		=> Request::post('num'),
                'sort'		=> Request::post('sort'),
				'status'	=> Request::post('status'),
				'update_time'	=> date('Y-m-d H:i:s')
			];

			$rs = Article::link('article_cats')
                ->where(['id' => $id])
                ->updateVal($article_cats)
                ->update();

			if ($rs !== FALSE) {
				Response::redirect('添加成功!', CONTROLLER_URL.'listArticleType');
			} else {
				Response::redirect(ACTION_URL, 2, '抱歉, 因网络原因编辑失败');
			}
		}
		View::$arrTplVar['catid'] = $id;
		View::show('article_type_edit');
	}

	/**
	 * desc 文章分类详情接口
	 */
	public function articleType()
	{
		$id = Request::post('id');
		$article_cats = Article::link('article_cats')->where(['id' => $id])->getOne();
		Response::success($article_cats);
	}

	/**
	 * desc 文章分类列表
	 */
	public function listArticleType()
	{
		View::show('article_type_list');
	}

	/**
	 * desc 文章分类接口
	 */
	public function articleTypelist()
	{
		$rs = Article::link('article_cats')
			->whereGT('status' , Admin::$Status_Del)
			->order('create_time desc')
			->select()
            ->getAll();
		Response::success( $rs);
	}

	/**
	 * desc 删除文章分类
	 */
	public function delArticleType()
	{
		$id = Request::post('id');

		empty($id) && exit('0');

		$count = Article::link('article')->where(['cat_id' => $id, 'status' => '>'. Admin::$Status_Del])->getCount();
		if (!empty($count)) {
			//这里会被ajax请求, 并不会跳转
			Response::redirect('此分类下还有未删除的文章, 不能删除此分类', CONTROLLER_URL.'listArticleType');
		}

		$rs = Article::link('article_cats')
			->where(['id' => $id])
			->updateVal(['status' => Admin::$Status_Del])
            ->update();

		if ($rs !== FALSE) {
			exit('1');
		} else {
			exit('0');
		}
	}

	/**
	 * desc 添加文章
	 */
	public function addArticle()
	{
		if (IS_POST) {
			$article = [
				'title'		=> Request::post('title'),
				'cat_id'	=> Request::post('cat_id'),
				'status'	=> Request::post('status'),
				'top'		=> Request::post('top'),
				'content'	=> nl2br(Request::post('content')),
				'create_time' => Request::post('create_time')
			];
			$this->verify($article);

			$articleid = Article::link('article')->insert($article);

			if ($articleid !== FALSE) {
				Article::link('tiezi')->insert(['id' => $articleid, 'rootid' => $articleid]);
				Response::redirect('添加成功!', CONTROLLER_URL.'listArticle');
			} else {
				Response::redirect('抱歉, 因网络原因添加失败', ACTION_URL);
			}

		}

		//默认时间
		View::$arrTplVar['defaultTime']= date('Y-m-d H:i:s');

		View::show('article_add');
	}

	/**
	 * desc 获取文章置顶选项
	 */
	public function toplist()
	{
		Response::success(Admin::$Top);
	}

	/**
	 * desc 编辑文章
	 */
	public function editArticle()
	{
		$articleId = Request::post('id');
		if (IS_POST) {
			$article = [
				'title'		=> Request::post('title'),
				'cat_id'	=> Request::post('cat_id'),
				'status'	=> Request::post('status'),
				'top'		=> Request::post('top'),
                'template'	=> Request::post('template', 1),
				'content'	=> nl2br(Request::post('content')),
				'create_time' => Request::post('create_time', date('Y-m-d H:i:s')),
				'update_time' => date('Y-m-d H:i:s')
			];

			$this->verify($article);

			$rs = Article::link('article')
                ->where(['id' => $articleId])
                ->updateVal($article)
                ->update();

			if ($rs !== FALSE) {
				Response::redirect('编辑成功!', CONTROLLER_URL.'listArticle');
			} else {
				Response::redirect('抱歉, 因网络原因编辑失败', ACTION_URL);
			}
		}
		View::$arrTplVar['articleid'] = $articleId;
		View::show('article_edit');
	}

	/**
	 * desc 文章详情接口
	 */
	public function articleDetail()
	{
		$articleId = Request::post('articleid');
		$article = Article::link('article')
			->where(['id' => $articleId])
			->getOne();

		$article['content'] = $this->formatArticle($article['content']);
		Response::success($article);
	}

	/**
	 * desc 文章列表
	 */
	public function listArticle()
	{
		View::show('article_list');
	}

	/**
	 * desc 文章列表接口
	 */
	public function articlelist()
	{
		$rs = Article::link('article')
			->whereGT('status' , Admin::$Status_Del)
			->order('status desc, top asc, create_time desc')
			->select()
            ->getAll();
		Response::success($rs);
	}

	/**
	 * desc 删除文章
	 */
	public function delArticle()
	{
		$id = Request::post('id');

		empty($id) && exit('删除失败, 未找数据!');

		$rs = Article::link('article')
			->where(['id' => $id])
			->updateVal(['status' => Admin::$Status_Del])
            ->update();

		if ($rs !== FALSE) {
			exit('1');
		} else {
			exit('0');
		}
	}
}
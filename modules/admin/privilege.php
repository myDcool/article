<?php
/**
 * desc 权限管理
 */
class _privilege extends BaseAdmin
{
	/**
	 * desc 添加角色
	 */
    public function addRole()
    {
    	if (IS_POST) {
			$role = [
				'name' => Request::post('name'),
				'access' => json_encode(Request::post('ctrl')),
				'status' => Admin::$Status_Ok,
				'create_time' => date('Y-m-d H:i:s')
			];

			$rs = Role::link('role')->insert($role);

			if ($rs !== FALSE) {
				Response::redirect('添加成功!', CONTROLLER_URL.'listRole');
			} else {
				Response::redirect('抱歉, 因网络原因添加失败', ACTION_URL);
			}
		}
        $rs = Fun::getAllController();
		View::$arrTplVar['func_list']= json_encode($rs);
        View::show('role_add');
    }

	/**
	 * desc 编辑角色
	 */
	public function editRole()
	{
		$id = Request::post('id');
		if (IS_POST) {
			$role = [
				'name' => Request::post('name'),
				'access' => json_encode(Request::post('ctrl')),
				'status' => Request::post('status'),
				'update_time' => date('Y-m-d H:i:s')
			];

			$rs = Role::link('role')
                ->where(['id' => $id])
                ->updateVal($role)
                ->update();

			if ($rs !== FALSE) {
				Response::redirect('编辑成功!', CONTROLLER_URL.'listRole');
			} else {
				Response::redirect('抱歉, 因网络原因编辑失败', CONTROLLER_URL.'listRole');
			}
		}

		$rs = Fun::getAllController();
		View::$arrTplVar['func_list']= json_encode($rs);

		$role = Role::link('role')->where(['id' => $id])->getOne();
		View::$arrTplVar['access']= $role['access'];
		unset($role['access']);
		View::$arrTplVar['role']= json_encode($role);

		View::show('role_edit');
	}

	/**
	 * desc 角色列表
	 */
	public function listRole()
	{
		View::show('role_list');
	}

	/**
	 * desc 角色列表接口
	 */
	public function rolelist()
	{
		$rs = Role::link('role')
			->where(['status' => '>'.Admin::$Status_Del])
			->select()
            ->getAll();
		Response::success($rs);
	}

	/**
	 * desc 删除角色
	 */
	public function delRole()
	{
		$id = Request::post('id');

		empty($id) && Response::error('删除失败, 未找数据!');

		Role::link('role')
			->where(['id' => $id])
			->updateVal(['status' => Admin::$Status_Del, 'access' => ''])
            ->update();

		Response::success();
	}

	/**
	 * desc 分配角色
	 */
	public function bindRole()
	{
		if (IS_POST) {
			$mobile = Request::post('mobile');
			$user = User::getTradeUserInfoByMobile($mobile);

			if (empty($user['uid'])) {
				Response::redirect('用户不存在!', ACTION_URL);
			} else {
				$bind = [
					'uid' => $user['uid'],
					'username' => $user['username'],
					'status' => Request::post('status'),
					'roleid' => Request::post('roleid'),
					'create_time' => date('Y-m-d H:i:s')
				];
				$rs = Role::link('role_bind')->replace($bind); //replace into

				if ($rs !== FALSE) {
					Response::redirect('编辑成功!', ACTION_URL);
				} else {
					Response::redirect('抱歉, 因网络原因添加失败', ACTION_URL);
				}
			}
		}

		$role = Role::link('role')
            ->where(['status' => Admin::$Status_Ok])
            ->select()
            ->getAll();
		View::$arrTplVar['role']= json_encode($role);
		View::show('role_bind');
    }

	/**
	 * desc 角色绑定列表
	 */
	public function listBindRole()
	{
		View::show('role_bind_list');
	}

	/**
	 * desc 角色列表接口
	 */
	public function bindlist()
	{
		$rs = Role::link('role_bind')
            ->joinFields('role_bind', 'id, username, status, create_time')
            ->joinFields('role', 'name')
            ->joinTable('role_bind', 'roleid', 'role', 'id')
			->whereGT('role_bind.status', Admin::$Status_Del)
			->select()
            ->getAll();
		Response::success($rs);
	}

	/**
	 * desc 删除角色绑定
	 */
	public function delBindRole()
	{
		$id = Request::post('post.id');

		empty($id) && exit('删除失败, 未找数据!');

		$rs = Role::link('role_bind')
			->where(['id' => $id])
			->updateVal(['status' => Admin::$Status_Del])
            ->update();

		if ($rs !== FALSE) {
			exit('1');
		} else {
			exit('0');
		}
	}

}
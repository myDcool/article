<?php
/**
 * desc 上传功能
 * Class Upload
 */
class _upload extends BaseAdmin
{
	/**
	 * desc 文章编辑器图片上传功能
	 * 百度ueditor编辑器调用,
	 * 对应js配置项为serverUrl
	 */
	public function ueUpload()
	{
		$arg = Request::post('action');
		switch ($arg) {
			case 'config':
				$config = [
					/* 上传图片配置项 */
					'imageActionName' =>  'ueUploadImage', /* 执行上传图片的action名称 */
					'imageFieldName' =>  'ueUpfile', /* 提交的图片表单名称 */
					'imageMaxSize' =>  2048000, /* 上传大小限制，单位B */
					'imageAllowFiles' =>  ['.png', '.jpg', '.jpeg', '.gif', '.bmp'], /* 上传图片格式显示 */
					'imageCompressEnable' =>  true, /* 是否压缩图片,默认是true */
					'imageCompressBorder' =>  1600, /* 图片压缩最长边限制 */
					'imageInsertAlign' =>  'none', /* 插入的图片浮动方式 */
					'imageUrlPrefix' =>  '', /* 图片访问路径前缀 */
					/* 截图工具上传 */
					'snapscreenActionName' =>  'ueUploadImage', /* 执行上传截图的action名称 */
					'snapscreenUrlPrefix' =>  '', /* 图片访问路径前缀 */
					'snapscreenInsertAlign' =>  'none', /* 插入的图片浮动方式 */
				];
				exit(json_encode($config));
				break;
			case 'ueUploadImage': //这个值对应上个case中的ueUploadImage
				$arr = QiNiu::getInstance()->upImage('ueUpfile', 'ueditor'); //ueUpfile 对应上个case中的ueUpfild
				if (QiNiu::getInstance()->isOk()) {
					$rs = [
						'state' => 'SUCCESS',
						'url' => $arr['qiniu_url'],
						'title' => '',
						'original' => ''
					];
					Fun::jsonReturn($rs);
				} else {
					$rs = [
						'state' => QiNiu::getInstance()->errMsg(),
					];
					Fun::jsonReturn($rs);
				}
				break;
			default:
				exit('none');
		}
	}

	/**
	 * desc umeditor 上传图片
	 */
	public function umUpload()
	{
		$arr = QiNiu::getInstance()->upImage('umUpfile', 'ueditor'); //umUpfile 表单名字
		if (QiNiu::getInstance()->isOk()) {
			$rs = [
				'state' => 'SUCCESS',
				'url' => $arr['qiniu_url'],
			];
		} else {
			$rs = [
				'state' => QiNiu::getInstance()->errMsg(),
			];
		}

		$callback = Request::post('callback');

		if($callback) {
			echo '<script>'.$callback.'('.json_encode($rs).')</script>';
		} else {
			echo json_encode($rs);
		}
	}
}

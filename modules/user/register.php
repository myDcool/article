<?php

class _register
{
	public function initc(){}

	public function index()
	{
		if (IS_POST) {
//			Safe::Check_Token_Once(); //防止重复提交

			$username = Request::post('username');
			if (mb_strlen($username) > 16) {
                Response::redirect('抱歉~ 您输入的姓名不能超过16个字符, 请重新输入', ACTION_URL, 3);
			}

			$mobile = Request::post('mobile', '');
			$password = Request::post('password');
			$password_confirm = Request::post('password_confirm');
			$remember_me  = Request::post('remember_me');

			if ( strcmp($password, $password_confirm) != 0) {
                Response::redirect('您输入的两个密码不一样, 请重新输入', ACTION_URL, 3);
			} else {
				$password = password_hash($password, PASSWORD_BCRYPT);
			}

			if ($mobile) {
				$mobile = password_hash($mobile, PASSWORD_BCRYPT);
			}

			$expire = $remember_me ? 86400*7 : 86400;

			$user = array(
				'username' => $username,
				'password' => $password,
				'mobile' => $mobile,
				'reg_from' => '',
				'addtime' => REQUEST_TIME
			);

			//入库
			$uid = User::link('user')->insert($user)->insertId;
			$user = ['username' => $username, 'mobile' => $mobile, 'uid' => $uid];
			User::setUserCookie($user, $expire);
            
            Response::redirect('注册成功~', BASEURL, 2);
		} else {
			View::show('reg');
		}
	}
}